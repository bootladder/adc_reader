extern crate adc_init;
extern crate time; 
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::thread;
use std::time::Duration;
use std::process::Command; use std::mem; 

fn main() {
  let mut x = 0 ;

//while 1==1 {
//  adc_init::ram_test();
//}
//

    if ! adc_init::is_adc_enabled()
    {
      adc_init::adc_init_complete();
    }
    //return;
  while 1==1 //x < 1000
  {
    x = x+1;
    let fifteensecond_millis = Duration::from_millis(1000);
    thread::sleep(fifteensecond_millis);

    println!("Reading ADC Data from PA15, AIN7");
    let sensor1data:u32 = read_sensor_1();
    append_csv_log(1,1,sensor1data);
    call_storer_shell_script(1,1,sensor1data);
    
    let fifteensecond_millis = Duration::from_millis(1000);
    thread::sleep(fifteensecond_millis);

    println!("Reading ADC Data from PA02, AIN0");
    let sensor2data:u32 = read_sensor_2();
    append_csv_log(1,2,sensor2data);

    let fifteensecond_millis = Duration::from_millis(1000);
    thread::sleep(fifteensecond_millis);

    println!("Reading ADC Data from tempsensor");
    let sensor5data:u32 = read_sensor_5();
    append_csv_log(1,5,sensor2data);
  }
}

fn read_sensor_1() -> u32 {

  adc_init::set_adc_input_ain7_pa15();
  let sensordata:u32 = adc_init::call_adcread_funit();
  return sensordata;
}
fn read_sensor_2() -> u32 {

  adc_init::set_adc_input_ain0_pa02();
  let sensordata:u32 = adc_init::call_adcread_funit();
  return sensordata;
}
fn read_sensor_5() -> u32 {

  adc_init::set_adc_input_tempsensor();
  let sensordata:u32 = adc_init::call_adcread_funit();
  return sensordata;
}


fn append_csv_log(deviceID:u8,sensorID:u8,sensorData:u32) {

    let mut file =
        OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open("/home/adc_data/data1.csv")
        .unwrap();

    if let Err(e) = writeln!(file, "{},{},{},{}",time::now().ctime(), deviceID,sensorID,sensorData) {
        println!("{}", e);
    }

}
fn call_storer_shell_script(deviceID:u8,sensorID:u8,sensorData:u32) {
    //Shell script with hard coded path here.
      let output = Command::new("./SensorDataEntry.out")
                  .arg(deviceID.to_string())
                  .arg(sensorID.to_string())
                  .arg(sensorData.to_string())
                  .output()
                  .expect("command failed to start");

    //print_bytes_vecu8_4(&output.stdout);
}
